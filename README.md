# Movieclub

Movieclub is an app to show you some interesting movies and their details. You can also filter the movies by:

- Keywords (valid search terms are title, year, and genre)
- Genre
- Min rating
- Max rating

This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app). `getServerSideProps()` is used to fetch movie/genre data from a local server and render the page using Server Side Rendering, preventing the user from seeing any loading spinners.

For compatibility with Server Side Rendering, I would recommend hosting providers such as Vercel or Netlify for ease of deployment, but any hosting provider with support for Node will work.

## Getting Started

First, ensure the local API server is running:

```bash
cd server
yarn
yarn serve
```

Then in a new terminal, from the root directory, run the development server for the Next app:

```bash
yarn
yarn dev
```

Open [http://localhost:4000](http://localhost:4000) with your browser to see the result.
