import { Filters } from "../components/Filters";
import { Footer } from "../components/Footer";
import { MoviesList } from "../components/MoviesList";
import { useState } from "react";
import Head from "next/head";
import styles from "../styles/Home.module.css";
import { Header } from "../components/Header";

const Home = ({ movies, genres }) => {
  const [keywordsFilter, setKeywordsFilter] = useState("");
  const [genreFilter, setGenreFilter] = useState("");
  const [minRatingFilter, setMinRatingFilter] = useState("");
  const [maxRatingFilter, setMaxRatingFilter] = useState("");
  const [filteredMovies, setFilteredMovies] = useState(movies);

  const applyFilters = (event) => {
    event.preventDefault(); // Don't reload the page
    setFilteredMovies(
      movies.filter(
        (movie) =>
          (movie.title.toLowerCase().includes(keywordsFilter.toLowerCase()) || // If keywords partially match title (case insensitive)
            movie.releaseYear === keywordsFilter || // OR keywords exactly match release year
            movie.genreNames.find(
              (genre) => genre.toLowerCase() === keywordsFilter.toLowerCase() // OR keywords exactly match any genres (case insensitive)
            )) &&
          (genreFilter === "" || movie.genreNames.includes(genreFilter)) && // Genre filters
          (minRatingFilter === "" ||
            movie.vote_average >= parseFloat(minRatingFilter)) && // Min rating filters
          (maxRatingFilter === "" ||
            movie.vote_average <= parseFloat(maxRatingFilter)) // Max rating filters
      )
    );
  };

  const resetFilters = (event) => {
    event.preventDefault(); // Don't reload the page
    setKeywordsFilter("");
    setGenreFilter("");
    setMinRatingFilter("");
    setMaxRatingFilter("");
    setFilteredMovies(movies);
  };

  return (
    <div className={styles.container}>
      <Head>
        <title>Movieclub</title>
        <meta
          name="description"
          content="Movieclub | Jonathan Wu FED Test Tundra"
        />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Header />
      <Filters
        keywordsFilter={keywordsFilter}
        setKeywordsFilter={setKeywordsFilter}
        applyFilters={applyFilters}
        genres={genres}
        genreFilter={genreFilter}
        setGenreFilter={setGenreFilter}
        minRatingFilter={minRatingFilter}
        setMinRatingFilter={setMinRatingFilter}
        maxRatingFilter={maxRatingFilter}
        setMaxRatingFilter={setMaxRatingFilter}
        resetFilters={resetFilters}
      />
      <div className={styles.count}>
        Showing {filteredMovies.length} results
      </div>
      <MoviesList movies={filteredMovies} />
      <Footer />
    </div>
  );
};

export async function getServerSideProps() {
  const moviesRaw = await fetch("http://localhost:3000/movies").then((res) =>
    res.json()
  );
  const genresRaw = await fetch("http://localhost:3000/genres").then((res) =>
    res.json()
  );

  const movies = moviesRaw.map((movie) => ({
    // Spread the existing movie data...
    ...movie,
    // But add a new key/value pair for genre names.
    // Map through the genre ids
    genreNames: movie.genre_ids.map(
      // And for each genre id, find the corresponding genre object...
      // Make sure to return only the name of the genre
      (genreId) => genresRaw.find((genre) => genre.id === genreId).name
    ),
    releaseYear: movie.release_date.substring(0, 4),
  }));

  const genres = genresRaw.map((genre) => genre.name); // We only need the genre names for now

  // Pass data to the page via props
  return { props: { movies, genres } };
}

export default Home;
