import styles from "./Filters.module.css";

const RatingFilterOptions = () => (
  <>
    <option value="">Any</option>
    {[1, 2, 3, 4, 5, 6, 7, 8, 9, 10].map((rating) => (
      <option key={rating} value={rating}>
        {rating}
      </option>
    ))}
  </>
);

const Filters = ({
  applyFilters,
  keywordsFilter,
  setKeywordsFilter,
  genres,
  genreFilter,
  setGenreFilter,
  minRatingFilter,
  setMinRatingFilter,
  maxRatingFilter,
  setMaxRatingFilter,
  resetFilters,
}) => {
  return (
    <form className={styles.container}>
      <div>
        <div className={styles.label}>Keywords</div>
        <input
          type="text"
          value={keywordsFilter}
          onChange={(event) => setKeywordsFilter(event.target.value)}
          placeholder="e.g Star Wars"
        />
      </div>
      <div>
        <div className={styles.label}>Genre</div>
        <select
          value={genreFilter}
          onChange={(event) => setGenreFilter(event.target.value)}
        >
          <option value="">Any</option>
          {genres.map((genre) => (
            <option key={genre} value={genre}>
              {genre}
            </option>
          ))}
        </select>
      </div>
      <div>
        <div className={styles.label}>Min Rating</div>
        <select
          value={minRatingFilter}
          onChange={(event) => setMinRatingFilter(event.target.value)}
        >
          <RatingFilterOptions />
        </select>
      </div>
      <div>
        <div className={styles.label}>Max Rating</div>
        <select
          value={maxRatingFilter}
          onChange={(event) => setMaxRatingFilter(event.target.value)}
        >
          <RatingFilterOptions />
        </select>
      </div>
      <button onClick={applyFilters}>Apply Filters</button>
      <button onClick={resetFilters}>Reset Filters</button>
    </form>
  );
};

export { Filters };
