import styles from "./Footer.module.css";

const Footer = () => (
  <footer className={styles.footer}>
    <div>© Copyright MovieClub Pty. Ltd. 2022</div>
    <img src="/logo-grey.svg" alt="Movieclub logo" />
  </footer>
);

export { Footer };
