import styles from "./Header.module.css";

const Header = () => (
  <header className={styles.header}>
    <img src="/logo.svg" alt="Movieclub logo" />
  </header>
);

export { Header };
