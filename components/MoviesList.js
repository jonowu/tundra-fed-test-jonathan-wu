import { MovieCard } from "./MovieCard";
import styles from "./MoviesList.module.css";

const MoviesList = ({ movies }) => (
  <div className={styles.grid}>
    {movies.map((movie) => (
      <MovieCard
        key={movie.id}
        title={movie.title}
        runtime={movie.runtime}
        releaseYear={movie.releaseYear}
        genres={movie.genreNames}
        posterPath={movie.poster_path}
        voteAverage={movie.vote_average}
      />
    ))}
  </div>
);

export { MoviesList };
