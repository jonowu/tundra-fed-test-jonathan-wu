import styles from "./MovieCard.module.css";

const MovieCard = ({
  genres,
  posterPath,
  releaseYear,
  runtime,
  title,
  voteAverage,
}) => (
  <div className={styles.card}>
    <div className={styles.posterContainer}>
      <img src={posterPath} alt={title} width="200" className={styles.poster} />
      <div className={styles.rating}>
        <img src="/star.svg" alt="star" className={styles.star} />
        {voteAverage}
      </div>
    </div>
    <div className={styles.content}>
      <div className={styles.title}>
        {title} ({releaseYear})
      </div>
      <div className={styles.meta}>
        {genres.join(", ")} • {runtime}
      </div>
    </div>
  </div>
);

export { MovieCard };
